package br.com.pablo.salesapp.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import br.com.pablo.salesapp.R;
import br.com.pablo.salesapp.models.Sale;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SaleAdapter extends RecyclerView.Adapter<SaleAdapter.ItemHolder> {

    private List<Sale> mSales;

    public SaleAdapter(List<Sale> sales) {
        mSales = sales;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SaleAdapter.ItemHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sale_adapter_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        holder.bindSale(mSales.get(position));
    }

    @Override
    public int getItemCount() {
        return mSales != null ? mSales.size() : 0;
    }

    public void refreshList(List<Sale> sales){
        mSales.clear();
        mSales.addAll(sales);
        notifyDataSetChanged();
    }

    public void clear(){
        mSales.clear();
        notifyDataSetChanged();
    }

    static class ItemHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.sale_adapter_view_tvReceipt) TextView tvReceipt;
        @BindView(R.id.sale_adapter_view_tvStatus) TextView tvStatus;
        @BindView(R.id.sale_adapter_view_tvAmount) TextView tvAmount;

        ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindSale(Sale sale) {
            tvReceipt.setText(sale.getReceipt());
            tvStatus.setText(sale.getStatus());
            String sAmount = String.format(Locale.getDefault(),"%.2f", sale.getAmount()) ;
            tvAmount.setText(sAmount);
        }
    }
}
