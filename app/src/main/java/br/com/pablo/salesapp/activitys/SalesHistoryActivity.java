package br.com.pablo.salesapp.activitys;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.JsonObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.pablo.salesapp.R;
import br.com.pablo.salesapp.adapters.SaleAdapter;
import br.com.pablo.salesapp.models.Cashier;
import br.com.pablo.salesapp.models.Sale;
import br.com.pablo.salesapp.retrofit.config.RetrofitConfig;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import retrofit2.Call;
import retrofit2.Callback;

public class SalesHistoryActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar mToobar;
    @BindView(R.id.sales_history_coordinatorLayout) ConstraintLayout mConstraintLayout;
    @BindView(R.id.sales_history_recycler_view_sales) RecyclerView mRecyclerViewSales;
    @BindView(R.id.sales_history_edDateStart) EditText mEditTextDateStart;
    @BindView(R.id.sales_history_edDateEnd) EditText mEditTextDateEnd;
    @BindView(R.id.sales_history_spinnerCashier) Spinner mSpinnerCashier;
    @BindView(R.id.sales_history_spinnerStatus) Spinner mSpinnerStatus;
    @BindView(R.id.sales_history_btnClear) Button mButtonClear;
    @BindView(R.id.sales_history_btnApply) Button mButtonApply;

    private SaleAdapter mAdapterSales;
    private List<Sale> mSale;
    private List<Cashier> mCashier = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_history);
        ButterKnife.bind(this);

        setSupportActionBar(mToobar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getResources().getString(R.string.activity_title_sales_history));
        }

        setupRecyclerSalesList();
        sendRequestListCashiers();
        loadSpinnerSalesStatus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupRecyclerSalesList() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerViewSales.setLayoutManager(layoutManager);

        mAdapterSales = new SaleAdapter(new ArrayList<Sale>(0) );
        mRecyclerViewSales.setAdapter(mAdapterSales);
    }

    @Optional
    @OnClick({R.id.sales_history_edDateStart, R.id.sales_history_edDateEnd})
    void clickSelectDate(EditText editText) {
        selectDate(editText);
    }

    @Optional
    @OnClick({R.id.sales_history_btnClear})
    void clickCleanFilterFields() {
        dialogConfirmCleanFilterFields();
    }

    @Optional
    @OnClick({R.id.sales_history_btnApply})
    void clicksendRequestListSales() {
        JsonObject request = new JsonObject();

        if(!mEditTextDateStart.getText().toString().isEmpty()){
            request.addProperty("date_start", formatDateToSend(mEditTextDateStart.getText().toString()));
        }

        if(!mEditTextDateEnd.getText().toString().isEmpty()){
            request.addProperty("date_end", formatDateToSend(mEditTextDateEnd.getText().toString()));
        }

        if(!mSpinnerCashier.getItemAtPosition(mSpinnerCashier.getSelectedItemPosition()).toString().isEmpty()
                && !mSpinnerCashier.getItemAtPosition(mSpinnerCashier.getSelectedItemPosition()).toString().equals("Todos")){
            request.addProperty("cashier", mSpinnerCashier.getItemAtPosition(mSpinnerCashier.getSelectedItemPosition()).toString());
        }

        if(!mSpinnerStatus.getItemAtPosition(mSpinnerStatus.getSelectedItemPosition()).toString().isEmpty()
                && !mSpinnerStatus.getItemAtPosition(mSpinnerStatus.getSelectedItemPosition()).toString().equals("Todos")){
            request.addProperty("status", mSpinnerStatus.getItemAtPosition(mSpinnerStatus.getSelectedItemPosition()).toString());
        }
        sendRequestListSales(request);

    }

    public String formatDateToSend (String inputDate){

        Date parsed;
        String outputDate = null;

        SimpleDateFormat df_input = new SimpleDateFormat("dd/MM/yy", java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException ignored) { }

        return outputDate;

    }

    private void selectDate(final EditText editText) {

        Calendar calendar = Calendar.getInstance();
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH) + 1;
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {

                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(0);
                    cal.set(year, month, day, 0, 0, 0);
                    Date chosenDate = cal.getTime();

                    DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
                    String formattedDate = df.format(chosenDate);

                    editText.setText(formattedDate);
                }
            };

            new DatePickerDialog(this, myDateSetListener, mYear, (mMonth - 1), mDay).show();
    }

    private void loadSpinnerSalesStatus() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.sales_status, R.layout.custom_simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mSpinnerStatus.setAdapter(adapter);
    }

    private void loadSpinnerSalesCashier() {
        ArrayAdapter<Cashier> adapter = new ArrayAdapter<>(
                this, R.layout.custom_simple_spinner_item, mCashier);
        Cashier cashier = new Cashier();
        cashier.setName("Todos");
        adapter.insert(cashier, 0);

        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mSpinnerCashier.setAdapter(adapter);
    }

    private void sendRequestListSales(JsonObject request){

        Call<List<Sale>> call = new RetrofitConfig().getSaleService().listSales(request);
        call.enqueue(new Callback<List<Sale>>() {
            @Override
            public void onResponse(Call<List<Sale>> call, retrofit2.Response<List<Sale>> response) {

                if(response.isSuccessful()){
                    if(response.code() == 204){
                        mAdapterSales.clear();
                        Snackbar snackbar = Snackbar
                                .make(mConstraintLayout, getResources().getString(R.string.msg_empty_content), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }else{
                        mSale = response.body();
                        mAdapterSales.refreshList(mSale);
                    }
                }else{
                    Snackbar snackbar = Snackbar
                            .make(mConstraintLayout, getResources().getString(R.string.msg_request_error), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<List<Sale>> call, Throwable t) {
                 Snackbar snackbar = Snackbar
                        .make(mConstraintLayout, getResources().getString(R.string.msg_failed_to_connect), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
    }

    private void sendRequestListCashiers(){

        Call<List<Cashier>> call = new RetrofitConfig().getCashierService().getCashier();
        call.enqueue(new Callback<List<Cashier>>() {
            @Override
            public void onResponse(Call<List<Cashier>> call, retrofit2.Response<List<Cashier>> response) {

                if(response.isSuccessful()){
                    if(response.code() == 204){
                        mAdapterSales.clear();
                        Snackbar snackbar = Snackbar
                                .make(mConstraintLayout, getResources().getString(R.string.msg_empty_content), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }else{
                        mCashier = response.body();
                    }
                }else{
                    Snackbar snackbar = Snackbar
                            .make(mConstraintLayout, getResources().getString(R.string.msg_request_error), Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

                loadSpinnerSalesCashier();
            }

            @Override
            public void onFailure(Call<List<Cashier>> call, Throwable t) {
                loadSpinnerSalesCashier();
                Snackbar snackbar = Snackbar
                        .make(mConstraintLayout, getResources().getString(R.string.msg_failed_to_connect), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });
    }

    private void dialogConfirmCleanFilterFields(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.dialog_confirm_clean_filter_fields_title));
        builder.setMessage(getResources().getString(R.string.dialog_confirm_clean_filter_fields_msg));

        builder.setPositiveButton(getResources().getString(R.string.dialog_confirm_clean_filter_fields_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                cleanFilterFields();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.dialog_confirm_clean_filter_fields_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void cleanFilterFields(){
        mEditTextDateStart.setText("");
        mEditTextDateEnd.setText("");
        mSpinnerCashier.setSelection(0);
        mSpinnerStatus.setSelection(0);
    }
}
