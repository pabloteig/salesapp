package br.com.pablo.salesapp.retrofit.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import br.com.pablo.salesapp.retrofit.service.Cashier;
import br.com.pablo.salesapp.retrofit.service.Sale;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {

    private final Retrofit retrofit;

    public RetrofitConfig() {

        Gson gson = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .create();

        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.10/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public Sale getSaleService() {
        return this.retrofit.create(Sale.class);
    }

    public Cashier getCashierService() {
        return this.retrofit.create(Cashier.class);
    }

}
