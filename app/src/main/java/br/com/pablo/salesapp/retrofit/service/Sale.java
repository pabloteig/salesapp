package br.com.pablo.salesapp.retrofit.service;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Sale {

    @POST("/salesAPI/public/api/listSales")
    Call<List<br.com.pablo.salesapp.models.Sale>> listSales(@Body JsonObject object);
}
