package br.com.pablo.salesapp.retrofit.service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.POST;

public interface Cashier {

    @POST("/salesAPI/public/api/cashiers")
    Call<List<br.com.pablo.salesapp.models.Cashier>> getCashier();
}